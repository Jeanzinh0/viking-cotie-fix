package com.jean.vikingcotiefix;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.items.ItemInteraction;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadItemsManagerEvent;
import com.jean.vikingcotiefix.Interactions.InteractionVikingCotie;

/**
 * Created by Jean on 28/02/2017.
 */
public class VikingCotie extends HabboPlugin implements EventListener
{

    @Override
    public void onEnable()
    {

        Emulator.getLogging().logStart("Loaded Viking Cotie!");

        Emulator.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable()
    {
        Emulator.getLogging().logShutdownLine("Disabled Viking Cotie Plugin.");
    }

    @EventHandler
    public static void onItemsLoading(EmulatorLoadItemsManagerEvent event)
    {
        Emulator.getGameEnvironment().getItemManager().addItemInteraction(new ItemInteraction("cotie", InteractionVikingCotie.class));
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }
}
